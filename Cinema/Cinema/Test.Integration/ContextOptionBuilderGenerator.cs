﻿
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace Test.Integration
{
    public class ContextOptionBuilderGenerator
    {
        public DbContextOptionsBuilder<CinemaContext> Build()
        {
            var optionBuilder = new DbContextOptionsBuilder<CinemaContext>();
            optionBuilder.UseSqlServer("Server=DESKTOP-MONHQ70;Database=bookdb;Trusted_Connection=True;");
            return optionBuilder;
        }
    }
}
