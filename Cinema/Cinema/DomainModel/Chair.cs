﻿using System;
using System.Collections.Generic;

namespace DomainModel
{
    public class Chair
    {
        public int Id { get; private set; }

        public int SalonId { get; private set; }

        public int Number { get; private set; }

        public int Row { get; private set; }

        public bool IsActive { get; private set; }

        public Guid ChairGuid { get; private set; }

        public virtual Salon Salon { get; private set; }

        public virtual List<ChairActivity> ChairActivities { get; private set; } = new List<ChairActivity>();

        public virtual List<Ticket> Tickets { get; private set; } = new List<Ticket>();
        private Chair() { }

        public static Chair Create()
        {
            throw new NotImplementedException();
        }
    }
}
