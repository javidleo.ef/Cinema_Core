﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel
{
    public class Ticket
    {
        public int Id { get; private set; }

        public int CustomerId { get; private set; }

        public int ChairId { get; private set; }

        public int SalonId { get; private set; }

        public int CinemaId { get; private set; }

        public decimal Price { get; private set; }

        public Guid TicketGuid { get; private set; }

        public virtual Chair Chair { get; private set; }

        public virtual Salon Salon { get; private set; }

        public virtual Cinema Cinema { get; private set; }

        public virtual Customer Customer { get; private set; }

        private Ticket() { }

        public static Ticket Create()
        {
            throw new NotImplementedException();
        }
    }
}
