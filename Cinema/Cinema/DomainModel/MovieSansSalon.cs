﻿using System;

namespace DomainModel
{
    public class MovieSansSalon
    {
        public int Id { get; private set; }

        public int MovieId { get; private set; }

        public int SansId { get; private set; }

        public int SalonId { get; private set; }

        public virtual Movie Movie { get; private set; }

        public virtual Salon Salon { get; private set; }

        public virtual Sans Sans { get; private set; }

        private MovieSansSalon() { }

        public static MovieSansSalon Create()
        {
            throw new NotImplementedException();
        }
    }
}
