﻿using System;
using System.Collections.Generic;

namespace DomainModel
{
    public class Movie
    {
        public int Id { get; private set; }

        public string Name { get; private set; }

        public string Director { get; private set; }

        public string Producer { get; private set; }

        public DateTime PublishDate { get; private set; }

        public string PublishDatePersian { get; private set; }

        public Guid MovieGuid { get; private set; }

        public virtual List<MovieSansSalon> MovieSansSalons { get; private set; } = new List<MovieSansSalon>();

        public virtual List<MovieActores> MovieActores { get; private set; } = new List<MovieActores>();

        private Movie() { }

        public static Movie Create()
        {
            throw new NotImplementedException();
        }
    }
}
