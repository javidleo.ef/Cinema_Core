﻿using Application.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using UseCases.Exceptions;
using UseCases.ServiceContract;

namespace Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViewersController : ControllerBase
    {
        private readonly IViewerService _service;

        public ViewersController(IViewerService service)
        {
            _service = service;
        }


        [HttpPost]
        public async Task<IActionResult> Post(CreateViewerDTO dto)
        {
            if (dto is null)
                throw new NotAcceptableException("invalid input");

            await _service.Create(dto.Name, dto.Family, dto.NationalCode, dto.Email, dto.DataOfBirth);
            return Ok();
        }

        [HttpGet("GetById/{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            if (id is 0)
                throw new NotAcceptableException("invalid id");

            var result = await _service.Find(id);
            return Ok(result);
        }

        [HttpGet("GetByName/{name}")]
        public async Task<IActionResult> GetByName(string name)
        {
            if (name is null)
                throw new NotAcceptableException("invalid input");

            var result = await _service.Find(name);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _service.GetAll();
            return Ok(result);
        }
    }
}
