﻿namespace Application.DTO;

public class CreateViewerDTO
{
    public string Name { get; set; }
    public string Family { get; set; }
    public string NationalCode { get; set; }
    public string Email { get; set; }
    public string DataOfBirth { get; set; }
}
