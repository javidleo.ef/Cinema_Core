﻿using DataAccess.Mapping;
using DomainModel;
using Microsoft.EntityFrameworkCore;

namespace DataAccess
{
    public class CinemaContext : DbContext
{
    public CinemaContext() { }
    public CinemaContext(DbContextOptions<CinemaContext> option) : base(option) { }


    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Apply Mappings 
        modelBuilder.ApplyConfiguration(new TicketMapping());
        modelBuilder.ApplyConfiguration(new ChairMapping());
        modelBuilder.ApplyConfiguration(new ChairActivityMapping());
        modelBuilder.ApplyConfiguration(new SalonMapping());
        modelBuilder.ApplyConfiguration(new SalonActivityMapping());
        modelBuilder.ApplyConfiguration(new CinemaMapping());
        modelBuilder.ApplyConfiguration(new CinemaActivityMapping());
        modelBuilder.ApplyConfiguration(new CustomerMapping());
        modelBuilder.ApplyConfiguration(new CityMapping());
        modelBuilder.ApplyConfiguration(new ProvinceMapping());
        modelBuilder.ApplyConfiguration(new MovieMapping());
        modelBuilder.ApplyConfiguration(new MovieActoresMapping());
        modelBuilder.ApplyConfiguration(new SansMapping());
        modelBuilder.ApplyConfiguration(new MovieSansSalonMapping());
    }
}
}