﻿using DomainModel;
using System.Collections.Generic;
using System.Linq;
using UseCases.RepositoryContract;

namespace DataAccess.Repository
{
    public class ViewerRepository : IViewerRepository
    {
        private readonly CinemaContext _context;
        public ViewerRepository() => _context = new CinemaContext();

        public void Add(Viewer viewer)
        {
            _context.Viewers.Add(viewer);
            _context.SaveChanges();
        }

        public bool DoesExist(string nationalCode)
        => _context.Viewers.Any(i => i.NationalCode == nationalCode);

        public Viewer Find(int id)
        => _context.Viewers.FirstOrDefault(i => i.Id == id);

        public Viewer Find(string natioanlCode)
        => _context.Viewers.FirstOrDefault(i => i.NationalCode == natioanlCode);

        public void Update(Viewer viewer)
        {
            _context.Viewers.Update(viewer);
            _context.SaveChanges();
        }

        public void Delete(Viewer viewre)
        {
            _context.Viewers.Remove(viewre);
            _context.SaveChanges();
        }

        public List<Viewer> GetAll()
        => _context.Viewers.ToList();
    }
}
