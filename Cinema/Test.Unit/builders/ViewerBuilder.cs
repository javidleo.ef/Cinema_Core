﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Unit.builders
{
    public class ViewerBuilder
    {
        private string _name = "ali";
        private string _family = "rezaie";
        private string _nationalCode = "0651534240";
        private string _email = "javidleo.ef@gmail.com";
        private string _dateofBirth = "11/12/1388";

        public ViewerBuilder WithName(string name)
        {
            _name = name;
            return this;
        }
        public ViewerBuilder WithFamily(string family)
        {
            _family = family;
            return this;
        }
        public ViewerBuilder WithNationalCode(string nationalCode)
        {
            _nationalCode = nationalCode;
            return this;
        }
        public ViewerBuilder WithEmail(string email)
        {
            _email = email;
            return this;
        }
        public ViewerBuilder WithDateOfBirth(string dateofBirth)
        {
            _dateofBirth = dateofBirth;
            return this;
        }
        public Viewer Build()
        {
            return Viewer.Create(_name,_family,_nationalCode,_email,_dateofBirth);
        }
    }
}
