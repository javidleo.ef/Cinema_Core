﻿Feature: Viewer
as a Viewer I want to be able buy ticket and watch movies in cinema

Background: 
	Given im a user in cinema website with following information
	| Email                 | Password |
	| javidloe.ef@gmail.com | 1234321  |

Scenario: Watch Movie when Having Ticket
	And i buy a ticket today for a movie 
	When i go to cinema 
	Then i should have premision to enter cinema and watch movie

Scenario: watch movie when do not have ticket
	And i did not buy ticket for movie 
	When i go to cinema 
	Then i should not have premision to cinema and watch movie

Scenario: watch movie when buy a ticket but movie has finished
	And i buy a ticket for a movie 
	When i go to cinema after the movie has finished 
	Then i dont have premision to enter the cinema and watch any movie

Scenario: sold out tickets
	And that movie i want to watch has sold out 
	When i try to buy a ticket 
	Then system give me sold out warning

Scenario: watch movie when buy a ticket buy movie has not started yet
	And i buy a ticket for a movie 
	When i go to cinema before movie start
	Then i should not have premision to enter and watch any movie